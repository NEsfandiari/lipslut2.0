import HomePageFeatured from './HomePageFeatured.jsx'
import HomePageMission from './HomePageMission.jsx'
import HomePageEmailForm from './HomePageEmailForm.jsx'
import Footer from './Footer.jsx'
import Navbar from './Navbar.jsx'
import Product from './Product.jsx'
import ProductMedia from './ProductMedia.jsx'
import CheckoutForm from './CheckoutForm.jsx'
import CartSidebar from './CartSidebar.jsx'
import MobileSidebar from './MobileSidebar'
import AccountDetails from './AccountDetails.jsx'
import OrderHistory from './OrderHistory.jsx'

export default {
  HomePageFeatured,
  HomePageMission,
  HomePageEmailForm,
  Footer,
  Navbar,
  Product,
  ProductMedia,
  CheckoutForm,
  CartSidebar,
  MobileSidebar,
  AccountDetails,
  OrderHistory,
}
