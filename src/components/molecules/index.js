import MissionStatement from './MissionStatement.jsx'
import FeaturedProduct from './FeaturedProduct.jsx'
import ProductPhotos from './ProductPhotos.jsx'
import ProductDescription from './ProductDescription.jsx'
import MissionPhilosophy from './MissionPhilosophy.jsx'
import SidebarItem from './SidebarItem.jsx'
import SummaryItem from './SummaryItem.jsx'
import SummaryFinancials from './SummaryFinancials.jsx'
import CheckoutShipping from './CheckoutShipping.jsx'
import CheckoutSummary from './CheckoutSummary.jsx'
import CheckoutPayment from './CheckoutPayment.jsx'
import Order from './Order.jsx'
import AccountDetailsList from './AccountDetailsList.jsx'
import AccountDetailsForm from './AccountDetailsForm.jsx'
import LoginEmailPassword from './LoginEmailPassword.jsx'
import LoginGoogle from './LoginGoogle.jsx'
import LoginFacebook from './LoginFacebook.jsx'
import SignupEmailPassword from './SignupEmailPassword.jsx'
import SignupGoogle from './SignupGoogle.jsx'
import SignupFacebook from './SignupFacebook.jsx'
import NavButtons from './NavButtons.jsx'

export default {
  MissionStatement,
  FeaturedProduct,
  ProductPhotos,
  ProductDescription,
  MissionPhilosophy,
  SidebarItem,
  SummaryItem,
  SummaryFinancials,
  CheckoutPayment,
  CheckoutShipping,
  CheckoutSummary,
  Order,
  AccountDetailsForm,
  AccountDetailsList,
  LoginEmailPassword,
  LoginGoogle,
  LoginFacebook,
  SignupEmailPassword,
  SignupGoogle,
  SignupFacebook,
  NavButtons,
}
